import Event from './components/Event/Event.vue';
import Weather from './components/Time/Weather.vue';
import Home from './components/Home.vue';
import Favorites from './components/Favorites.vue';

export const routes = [
    {path: '', component: Home},
    {path: '/Event', component: Event},
    {path: '/Weather', component: Weather},
    {path: '/Favorites', component: Favorites}
];