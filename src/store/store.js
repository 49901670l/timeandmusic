export const store = {
    state: {
        favoritos: []
    },
    getFavor(id) {
        return this.state.favoritos.find((favorito) => favorito.eventId === id);
    },
    setFavor(eventId, name, city) {
        if (!this.getFavor(eventId)){
            this.state.favoritos.push({eventId: eventId, name: name, city: city});
        }
    },
    setFavorDetail(eventId, name) {
        var event = this.getFavor(eventId);
        event.name = name;
    },
    getFavorList() {
        return this.state.favoritos;
    },
    setFavorRemove(id) {
        var event = this.getFavor(id);
        var indexOf =this.state.favoritos.indexOf(event);
        this.state.favoritos.splice(indexOf,1);
    }
};